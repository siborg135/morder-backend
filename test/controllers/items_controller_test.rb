require 'test_helper'

class ItemsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @item = items(:jessicasitem)
    @wilsonsorder = orders(:wilsonsorder)
    @jessicasorder = orders(:jessicasorder)
    @jessica = users(:jessica)
  end

  test "should get index" do
    get items_url, as: :json
    assert_response :success
  end

  test "should create item" do
    assert_difference('Item.count') do
      post items_url, params: { name: "Burger", order_id: @wilsonsorder.id, price: 1230, token: @jessica.token  }, as: :json
    end

    assert_response 201
  end

  test "should show item" do
    get item_url(@item), as: :json
    assert_response :success
  end

  test "should update item" do
    patch item_url(@item), params: { name: @item.name, order_id: @item.order_id, price: @item.price, token: @jessica.token }, as: :json
    assert_response 200
  end

  test "should destroy item" do
    assert_difference('Item.count', -1) do
      delete item_url(@item), params: { token: @item.user.token }, as: :json
    end

    assert_response 204
  end
end
