require 'test_helper'

class ItemTest < ActiveSupport::TestCase
  test "user can have only one item in a given Order" do
    user1 = User.new(name: "Adam", remote_id: 1234, remote: "facebook")
    order1 = Order.new(restaurant: "Sowa", status: "open", user: user1)
    order2 = Order.new(restaurant: "Przyjaciele", status: "open", user: user1)

    item1 = Item.new(name: "Beef Burger", price: 1234, user: user1, order: order1 )

    assert_nothing_raised  do
      item1.save!
    end

    item2 = Item.new(name: "Veggie Burger", price: 1234, user: user1, order: order1 )
    assert_raise ActiveRecord::RecordNotUnique do
      item2.save!
    end

    item3 = Item.new(name: "Veggie Burger", price: 1234, user: user1, order: order2 )
    assert_nothing_raised do
      item3.save!
    end

  end

  test "price cannot be non-positive and must be an integer" do
    user1 = User.new(name: "Adam", remote_id: 1234, remote: "facebook")
    order1 = Order.new(restaurant: "Sowa", status: "open", user: user1)
    item1 = Item.new(name: "Beef Burger", price: 1234, user: user1, order: order1 )

    assert item1.valid?

    item1.price = 0
    assert_not item1.valid?
    item1.price = -1
    assert_not item1.valid?
    item1.price = 1.1
    assert_not item1.valid?

  end
end
