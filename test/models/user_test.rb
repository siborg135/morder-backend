require 'test_helper'

class UserTest < ActiveSupport::TestCase


  test "remote ids bigger than 2^32 work" do
    fat_user = User.new(name: "fat guy", remote_id: 42949672960, remote: "facebook")
    assert fat_user.save
  end

  test "can handle large tokens" do
    token_user = User.new(name: "Large Token User", remote_id: 234212134, remote: "facebook", token: "EAAFmXtzP5awBAKZCzHZCZATP50SEfrbHpkBUeKTQAAZBSNMU4q95yl0FuRPxKZCWLkIlTveZATsyWX0ZAwbeawzhEvVpnmhNDt9fTgoUc7ZCTUkXENZCHyO0HHQBG4uK4ZCd2pV6tb5THcAwJLreMoq2gGT7ziil2jVkC0Cn1lZCMfucMw14pCEWreSMCFhu8xVliQZD", token_expires_at: DateTime.now+10.year )
    assert token_user.save
  end

  test "remote and remote_id combination is unique" do
    user1 = User.new(name: "Joe", remote_id: 1234, remote: "facebook")
    user1.save
    user2 = User.new(name: "Joe", remote_id: 1234, remote: "facebook")
    user3 = User.new(name: "Joe", remote_id: 5678, remote: "facebook")

    assert_raise ActiveRecord::RecordNotUnique do
      user2.save
    end

    assert_nothing_raised  do
      user3.save
    end



  end
end
