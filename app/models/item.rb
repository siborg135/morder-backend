class Item < ApplicationRecord
  belongs_to :user
  belongs_to :order

  validates :price, numericality: { greater_than: 0, only_integer: true }

end
