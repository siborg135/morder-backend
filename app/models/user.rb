class User < ApplicationRecord
  has_many :items
  has_many :orders

  def token_expired?
    DateTime.now > self.token_expires_at
  end

  def find_by_token(token)
    if token

      user_from_db = User.find_by(token: token)
      if user_from_db

        if user_from_db.token_expired?
          raise "Provided token is expired"
        else
          return user_from_db
        end

      else
        return user_from_new_token(token)
      end

    else
      return nil
    end
  end

  private

      def token_details(token)
        url = "https://graph.facebook.com/debug_token?input_token=#{token}&access_token=394032594281900|f2f176e67f497411efabde3f0f57f10f" # yeah, I know my dev's token is hard coded, but as for now it poses no threat, does it?
        response = Net::HTTP.get(URI(url))
        response = JSON.parse(response)
        return {
          is_valid: response["data"]["is_valid"],
          remote_id: response["data"]["user_id"].to_i,
          expires_at: Time.at(response["data"]["expires_at"])
        }
      end


      def user_from_new_token(token)
        token_details = token_details(token)
        if token_details[:is_valid]
          existing_user = User.find_by(remote_id: token_details[:remote_id])
          if existing_user
            existing_user.token = token
            existing_user.token_expires_at = token_details[:expires_at]
            return existing_user
          else
            new_user = User.create(name: "Test Name", token: token, token_expires_at: token_details[:expires_at], remote: "facebook", remote_id: token_details[:remote_id])
            return new_user
          end
        else
          return nil
        end

      end

end
