class ApplicationController < ActionController::API
  include Pundit
  before_action :set_user_by_token, :disable_cors

  def pundit_user
    @current_user
  end

  private
    def set_user_by_token
      @current_user = User.find_by_token(params['token'])
    end

    def disable_cors
      headers['Access-Control-Allow-Origin'] = '*'
      headers['Access-Control-Allow-Methods'] = 'POST, PUT, DELETE, GET, OPTIONS'
      headers['Access-Control-Request-Method'] = '*'
      headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
    end
end
