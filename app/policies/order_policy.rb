class OrderPolicy < ApplicationPolicy
  def index?
    true
  end

  def update?
    record.user == user # users can edit only their orders
  end

  def destroy?
    update?
  end

end
