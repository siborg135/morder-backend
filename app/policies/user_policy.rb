class UserPolicy < ApplicationPolicy
  def index?
    false
  end
  
  def create?
    false
  end

  def show?
    false
  end

  def update?
    false # you can't change users like that
  end

  def destroy?
    update?
  end

end
