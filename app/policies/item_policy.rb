class ItemPolicy < ApplicationPolicy
  def index?
    true
  end

  def update?
    record.user == user # users can edit only their items
  end

  def destroy?
    update?
  end

end
