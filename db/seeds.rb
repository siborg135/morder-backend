# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


User.create!([
  { name: 'First User', remote_id: 23423432, remote: "facebook", token: "first", token_expires_at: DateTime.now+10.year },
  { name: 'Second User', remote_id: 778883432, remote: "facebook", token: "second", token_expires_at: DateTime.now+10.year },
  { name: 'Third', remote_id: 1234, remote: "facebook", token: "third", token_expires_at: DateTime.now+10.year },
  { name: 'Expired', remote_id: 5678, remote: "facebook", token: "expired", token_expires_at: DateTime.now-10.year }
])


Order.create!([
  { restaurant: "Multifood", user: User.first, status: "open"},
  { restaurant: "Multifood", user: User.second, status: "open"}
])


Item.create!([
  { name: "Veggieburger", price: 2200, order: Order.first, user: User.first },
  { name: "Fritzcola", price: 1200, order: Order.first, user: User.second }
])
