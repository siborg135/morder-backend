class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :name
      t.integer :remote_id, limit: 8
      t.string :remote

      t.timestamps
    end

    add_index(:users, [:remote_id, :remote], unique: true)
  end
end
