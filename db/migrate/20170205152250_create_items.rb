class CreateItems < ActiveRecord::Migration[5.0]
  def change
    create_table :items do |t|
      t.string :name
      t.string :price
      t.references :user, foreign_key: true
      t.references :order, foreign_key: true

      t.timestamps
    end

    add_index(:items, [:order_id, :user_id], unique: true)

  end
end
